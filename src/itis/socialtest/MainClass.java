package itis.socialtest;

import java.io.*;
import java.util.*;

import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();



    public static void main(String[] args) {
        new MainClass().run("src\\itis\\socialtest\\resources\\PostDatabase.csv", "src\\itis\\socialtest\\resources\\Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        ArrayList<Post> allPosts = new ArrayList<>();
        try {
            ArrayList<Author> authors = new ArrayList<>();
            FileReader fileReader = new FileReader(authorsSourcePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 5; i++) {
                String[] strings = bufferedReader.readLine().split(", ");
                Author author = new Author(Long.parseLong(strings[0]), strings[1], strings[2]);
                authors.add(author);
            }
            bufferedReader.close();

            FileReader fileReader1 = new FileReader(postsSourcePath);
            BufferedReader bufferedReader1 = new BufferedReader(fileReader1);
            for (int i = 0; i < 9; i++) {
                String[] strings = bufferedReader1.readLine().split(", ");
                Post post = new Post(strings[2], strings[3], Long.parseLong(strings[1]), authors.get(Integer.parseInt(strings[0])));
                allPosts.add(post);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 9; i++) {
            System.out.println(allPosts.get(i).getContent() + ". || Posted by " + allPosts.get(i).getAuthor().getNickname() + ". At " + allPosts.get(i).getDate());
        }

        System.out.println();
        System.out.println();

        List<Post> arrayListOfPostsByDate;
        arrayListOfPostsByDate = analyticsService.findPostsByDate(allPosts,"17.04.2021");
        for (int i = 0; i < arrayListOfPostsByDate.size(); i++) {
            System.out.println(arrayListOfPostsByDate.get(i));
        };
        System.out.println();
        System.out.println();

        List<Post> arr = analyticsService.findAllPostsByAuthorNickname(allPosts, "varlamov");
        for (int i = 0; i < arr.size(); i++) {
            System.out.println(arr.get(i));
        }
        System.out.println();
        System.out.println();

        System.out.println(analyticsService.checkPostsThatContainsSearchString(allPosts,"Россия"));
        System.out.println();
        System.out.println();
    }
}
